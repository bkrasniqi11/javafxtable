import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Function;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import main.EditCell;
import beans.AnalysisPacientDetails;
import model.AnalysisGroupJpaController;
import model.AnalysisPacientDetailsJpaController;
import model.AnalysisPacientJpaController;


public class TableController implements Initializable {

    @FXML
    private Button button1;
    @FXML
    private TableView<AnalysisPacientDetails> tableResults;
    @FXML
    private TableColumn<AnalysisPacientDetails, String> colAnalysis;
    @FXML
    private TableColumn<AnalysisPacientDetails, String> colResults;

    protected EntityManagerFactory emf = Persistence.createEntityManagerFactory("analysisDatabase");
    private ObservableList listAnalysis = FXCollections.observableArrayList();
    private AnalysisPacientDetailsJpaController apdjpa = new AnalysisPacientDetailsJpaController(emf);
    private AnalysisPacientJpaController apjpa = new AnalysisPacientJpaController(emf);
    private AnalysisGroupJpaController agjpa = new AnalysisGroupJpaController(emf);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        listAnalysis.addAll(apdjpa.getByGroup(apjpa.findAnalysisPacient(118), agjpa.findAnalysisGroup(10)));
        tableResults.setItems(listAnalysis);

        colAnalysis.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AnalysisPacientDetails, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<AnalysisPacientDetails, String> param) {
                return new SimpleStringProperty((param.getValue()).getIdAnalysis().getAnalysisName());

            }
        });

        tableResults.getSelectionModel().setCellSelectionEnabled(true);
        tableResults.setEditable(true);

        tableResults.setOnKeyPressed(event -> {
            TablePosition<AnalysisPacientDetails, String> pos = tableResults.getFocusModel().getFocusedCell();
            if (pos != null && event.getCode().isLetterKey()) {
                tableResults.edit(pos.getRow(), pos.getTableColumn());
            }
        });

        tableResults.setOnMouseClicked((MouseEvent me) -> {
            if (me.getClickCount() == 1) {
                TablePosition focusedCellPosition = tableResults.getFocusModel().getFocusedCell();
                tableResults.edit(focusedCellPosition.getRow(), focusedCellPosition.getTableColumn());
            }
        });

//        Function<AnalysisPacientDetails, StringProperty> property = new Function<AnalysisPacientDetails, StringProperty>() {
//            @Override
//            public StringProperty apply(AnalysisPacientDetails t) {
//                return new SimpleStringProperty(t.getAnalysisPacientDetailsRezultat());
//            }
//        };
//        colRez.setCellValueFactory(cellData -> property.apply(cellData.getValue()));
        colResults.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AnalysisPacientDetails, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<AnalysisPacientDetails, String> param) {
                return new SimpleStringProperty((param.getValue()).getAnalysisPacientDetailsRezultat());
            }
        });

        colResults.setCellFactory(cc -> new EditCell<AnalysisPacientDetails, String>(new StringConverter<String>() {

            @Override
            public String toString(String object) {
                return object;
            }

            @Override
            public String fromString(String string) {
                AnalysisPacientDetails apdRez = (AnalysisPacientDetails) tableResults.getSelectionModel().getSelectedItem();
                short sh = 1;
                apdRez.setAnalysisPacientDetailsDone(sh);
                apdRez.setAnalysisPacientDetailsRezultat(string);
                try {
                    apdjpa.edit(apdRez);
                    TablePosition pos = tableResults.getFocusModel().getFocusedCell();                    
                } catch (Exception ex) {
                    ex.printStackTrace();
                }                
                return string;
            }
        }));
    }
}
