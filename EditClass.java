import beans.AnalysisPacientDetails;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.StringConverter;

public class EditCell<S, T> extends TableCell<S, T> {

    private final TextField textField = new TextField();

    private final StringConverter<T> converter;

    public EditCell(StringConverter<T> conv) {
        this.converter = conv;

        textField.setAlignment(Pos.CENTER);

        itemProperty().addListener((obx, oldItem, newItem) -> {
            if (newItem == null) {
                setText(null);
            } else {
                setText(converter.toString(newItem));
            }
        });

        setGraphic(textField);
        setContentDisplay(ContentDisplay.TEXT_ONLY);

        textField.setOnAction(evt -> {
            commitEdit(this.converter.fromString(textField.getText()));
        });
        textField.focusedProperty().addListener((obs, wasFocused, isNowFocused) -> {
            if (! isNowFocused) {
                commitEdit(this.converter.fromString(textField.getText()));
            }
        });
        textField.textProperty().bindBidirectional(textProperty());
        textField.setOnKeyPressed((KeyEvent event) -> {
            TablePosition focusedCellPosition = getTableView().getFocusModel().getFocusedCell();
            if (event.getCode() == KeyCode.ESCAPE) {
                textField.setText(converter.toString(getItem()));
                cancelEdit();
                event.consume();
            } else if (event.getCode() == KeyCode.TAB) {
                getTableView().edit(focusedCellPosition.getRow(), focusedCellPosition.getTableColumn());
            } else if (event.getCode() == KeyCode.ENTER) {
                event.consume();
            } else if (event.getCode() == KeyCode.UP) {
                event.consume();
            } else if (event.getCode() == KeyCode.DOWN) {
                event.consume();
            }

            TablePosition pos = getTableView().getFocusModel().getFocusedCell();

            if (pos.getColumn() == 3) {
                getTableView().getSelectionModel().clearAndSelect(pos.getRow() + 1, pos.getTableColumn());
            }
        });

        textField.setOnKeyReleased((KeyEvent event) -> {
            TablePosition pos = getTableView().getFocusModel().getFocusedCell();
            if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {

                if (pos.getRow() == getTableView().getItems().size() - 1) {
                    commitEdit(converter.fromString(textField.getText()));
                    event.consume();
                } else if (pos.getRow() < getTableView().getItems().size() - 1) {
                    commitEdit(converter.fromString(textField.getText()));
                    getTableView().getSelectionModel().selectBelowCell();
                    TablePosition focusedCellPosition = getTableView().getFocusModel().getFocusedCell();
                    getTableView().edit(focusedCellPosition.getRow(), focusedCellPosition.getTableColumn());
                    event.consume();
                }
            } else if (event.getCode() == KeyCode.UP) {

                if (pos.getRow() == getTableView().getItems().size() - 1) {
                    commitEdit(converter.fromString(textField.getText()));
                    event.consume();
                } else if (pos.getRow() < getTableView().getItems().size() - 1) {
                    commitEdit(converter.fromString(textField.getText()));
                    getTableView().getSelectionModel().selectAboveCell();
                    TablePosition focusedCellPosition = getTableView().getFocusModel().getFocusedCell();
                    getTableView().edit(focusedCellPosition.getRow(), focusedCellPosition.getTableColumn());
                    event.consume();
                }
            } else if (event.getCode() == KeyCode.DOWN) {
                if (pos.getRow() == getTableView().getItems().size() - 1) {
                    commitEdit(converter.fromString(textField.getText()));
                    event.consume();
                } else if (pos.getRow() < getTableView().getItems().size() - 1) {
                    commitEdit(converter.fromString(textField.getText()));
                    getTableView().getSelectionModel().selectBelowCell();
                    TablePosition focusedCellPosition = getTableView().getFocusModel().getFocusedCell();
                    getTableView().edit(focusedCellPosition.getRow(), focusedCellPosition.getTableColumn());
                    event.consume();
                }
            }
        });
    }

    public static final StringConverter<String> IDENTITY_CONVERTER = new StringConverter<String>() {

        @Override
        public String toString(String object) {
            return object;
        }

        @Override
        public String fromString(String string) {
            System.out.println(string);
            return string;
        }

    };

    // set the text of the text field and display the graphic
    @Override
    public void startEdit() {
        super.startEdit();
        textField.setText(converter.toString(getItem()));
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        textField.requestFocus();
    }

    // revert to text display
    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    // commits the edit. Update property if possible and revert to text display
    @Override
    public void commitEdit(T item) {
        if (!isEditing() && !item.equals(getItem())) {
            TableView<S> table = getTableView();
            if (table != null) {
                TableColumn<S, T> column = getTableColumn();
                TableColumn.CellEditEvent<S, T> event = new TableColumn.CellEditEvent<>(table,
                        new TablePosition<S, T>(table, getIndex(), column),
                        TableColumn.editCommitEvent(), item);
                Event.fireEvent(column, event);
            }
        }
        super.commitEdit(item);
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }
}
